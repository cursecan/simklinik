from django.core.exceptions import PermissionDenied

def isDokter(function):
    def wrap(request, *args, **kwargs):
        if not request.user.is_superuser:
            if request.user.user_lvl.code == '100' :
                return function(request, *args, **kwargs)
            else:
                raise PermissionDenied

        return function(request, *args, **kwargs)
        
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap