from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ex%z@a4ov0off2ey4mvj6d2(x^nysxdy5yvr-ww^7tkbpv8k9('

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}