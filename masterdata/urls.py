from django.urls import path

from . import views

app_name = 'masterdata'

urlpatterns = [
    path('obat-create/', views.ObatCreateView.as_view(), name='obat-create'),
    path('obat-list/', views.ObatListView.as_view(), name='obat-list'),
    path('obat/<int:pk>/', views.ObatDetailView.as_view(), name='obat-detail'),
    path('obat/<int:pk>/update/', views.ObatUpdateView.as_view(), name='obat-update'),
    path('obat/<int:id>/add/', views.addStokObat, name='add-stok-obat'),

    path('tarif-create/', views.TarifCreateView.as_view(), name='tarif-create'),
    path('tarif-list/', views.TarifListView.as_view(), name='tarif-list'),
    path('tarif/<int:pk>/', views.TarifDetailView.as_view(), name='tarif-detail'),
    path('tarif/<int:pk>/update/', views.TarifUpdateView.as_view(), name='tarif-update'),
    path('pasien-create/', views.PasienCreateView.as_view(), name='pasien-create'),
    path('pasien-list/', views.PasienListView.as_view(), name='pasien-list'),
    path('pasien/<int:pk>/', views.PasienDetailView.as_view(), name='pasien-detail'),
    path('pasien/<int:pk>/update/', views.PasienUpdateView.as_view(), name='pasien-update'),
    path('pasien-create-js/', views.pasienCreateJsView, name='pasien-create-js'),
]