from .models import Klinik

def current_klinik(request):
    klinik_objs = Klinik.objects.all()
    if klinik_objs.exists():
        return {'processor_klinik': klinik_objs.first()}

    else :
        return {'processor_klinik': None}