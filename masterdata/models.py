from django.db import models

from account.models import User
from core.models import ComondBase

from .utils import get_no_pasien

import pendulum
from datetime import date, datetime

class Pasien(ComondBase):
    PRIA = 'P'
    WANITA = 'W'
    KELAMIN_LIST = (
        (PRIA, 'Pria'),
        (WANITA, 'Wanita')
    )

    no_pasien = models.CharField(max_length=7, unique=True, editable=False)
    nm_pasien = models.CharField('Nama Lengkap', max_length=50)
    tmp_lahir = models.CharField('Tempat Lahir', max_length=90)
    tgl_lahir = models.DateField(verbose_name='Tanggal Lahir')
    alamat = models.CharField('Alamat', max_length=200)
    jns_kelamin = models.CharField('Jenis Kelamin', max_length=1, choices=KELAMIN_LIST)
    telepon = models.CharField('No. Telepon', max_length=16, blank=True)

    class Meta:
        ordering = [
            'nm_pasien', 'id'
        ]

    def save(self, *args, **kwargs):
        if self.no_pasien is None or self.no_pasien == '':
            self.no_pasien = get_no_pasien(self)

        super().save(*args, **kwargs)

    def __str__(self):
        return '{} - {}'.format(self.no_pasien, self.nm_pasien)

    def get_ages(self):
        return pendulum.instance(datetime.fromordinal(self.tgl_lahir.toordinal())).age

class Obat(ComondBase):
    barcode = models.CharField(max_length=30, unique=True)
    nm_obat = models.CharField(max_length=100)
    kandungan_obat = models.CharField(max_length=200, blank=True)
    harga_jual = models.PositiveIntegerField()
    stok = models.PositiveIntegerField(default=0, editable=False)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = [
            'nm_obat', 'id',
        ]

    def __str__(self):
        return self.nm_obat

    def inactive(self):
        return self.stok < 1 or (not self.active)


class Tarif(ComondBase):
    nm_tarif = models.CharField(max_length=100, unique=True)
    deskripsi = models.TextField(max_length=2000, blank=True)
    trf_dokter = models.PositiveIntegerField(default=0)
    trf_asisten = models.PositiveIntegerField(default=0)
    trf_klinik = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = [
            'nm_tarif', 'id'
        ]

    def __str__(self):
        return self.nm_tarif

    def get_subtotal(self):
        return self.trf_asisten + self.trf_dokter + self.trf_klinik


class Klinik(models.Model):
    nm_klinik = models.CharField(max_length=50)
    alamat = models.CharField(max_length=100, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = [
            'id'
        ]

    def __str__(self):
        return self.nm_klinik