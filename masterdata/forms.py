from django import forms
from django.conf import settings

from .models import (
    Pasien, Obat, Tarif, Klinik,
)

class StockObatForm(forms.Form):
    stok = forms.IntegerField(min_value=1)


class PasienForm(forms.ModelForm):
    tgl_lahir = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y'),
        input_formats=('%d/%m/%Y', )
    )
    class Meta:
        model = Pasien
        fields = '__all__'


class ObatForm(forms.ModelForm):
    class Meta:
        model = Obat
        fields = [
            'barcode', 'nm_obat',
            'kandungan_obat', 'harga_jual'
        ]


class TarifFrom(forms.ModelForm):
    class Meta:
        model = Tarif
        fields = '__all__'