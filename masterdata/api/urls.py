from rest_framework import routers

from . import views

app_name = 'api_masterdata'

routes = routers.SimpleRouter()
routes.register('pasien', views.PasienApiViewset, basename='pasien')
routes.register('obat', views.ObatApiViewset, basename='obat')
routes.register('tarif', views.TarifApiViewset, basename='tarif')

urlpatterns = routes.urls