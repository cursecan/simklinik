from rest_framework import mixins, viewsets
from django.db.models import Q

from .serializers import PasienSerializer, ObatSerializer, TarifSerializer

from masterdata.models import Pasien, Obat, Tarif


class TarifApiViewset(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = TarifSerializer
    def get_queryset(self):
        query = Tarif.objects.all()
        q = self.request.GET.get('q', None)

        if q:
            query = query.filter(
                nm_tarif__contains=q
            )
        return query

class PasienApiViewset(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = PasienSerializer

    def get_queryset(self):
        query = Pasien.objects.all()
        q = self.request.GET.get('q', None)

        if q:
            query = query.filter(
                Q(nm_pasien__contains=q) | Q(no_pasien__contains=q)
            )
        return query


class ObatApiViewset(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ObatSerializer

    def get_queryset(self):
        query = Obat.objects.all()
        q = self.request.GET.get('q', None)

        if q:
            query = query.filter(
                nm_obat__contains=q
            )
        return query

