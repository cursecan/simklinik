from rest_framework import serializers

from masterdata.models import (
    Pasien, Obat, Tarif,
)

class PasienSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pasien
        fields = [
            'id', 'nm_pasien', 'no_pasien',
        ]


class ObatSerializer(serializers.ModelSerializer):
    inactive = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Obat
        fields = [
            'id', 'nm_obat', 'inactive'
        ]

    def get_inactive(self, obj):
        return obj.inactive()

class TarifSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tarif
        fields = [
            'id', 'nm_tarif', 'deskripsi'
        ]