from django.contrib import admin

from .models import (
    Pasien, Tarif, Obat, Klinik,
)

@admin.register(Klinik)
class KlinikAdmin(admin.ModelAdmin):
    pass

@admin.register(Pasien)
class PasienAdmin(admin.ModelAdmin):
    pass


@admin.register(Tarif)
class TarisAdmin(admin.ModelAdmin):
    pass


@admin.register(Obat)
class ObatAdmin(admin.ModelAdmin):
    pass