from django.utils.crypto import get_random_string

import string


def get_no_pasien(instance, size=7, chars=string.digits):
    newcode = get_random_string(size, chars)

    instance_class = instance.__class__
    if instance_class.objects.filter(no_pasien=newcode).exists():
        return get_no_pasien(instance)

    return newcode