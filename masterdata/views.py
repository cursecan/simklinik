from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.postgres.search import SearchVector
from django.views.generic import (
    ListView, DetailView, 
    UpdateView, CreateView,
)

from .models import (
    Pasien, Obat, Tarif,
)

from .forms import (
    PasienForm, ObatForm, TarifFrom,
    StockObatForm,
)

@method_decorator(login_required, name='dispatch')
class PasienListView(ListView):
    template_name = 'masterdata/pasien-list-view.html'
    paginate_by = 10
    context_object_name = 'pasien_list'

    def get_queryset(self):
        query = Pasien.objects.all()
        q = self.request.GET.get('q', None)
        if q:
            query = query.annotate(
                search=SearchVector('nm_pasien', 'telepon', 'no_pasien')
            ).filter(search=q)

        return query

@method_decorator(login_required, name='dispatch')
class PasienDetailView(DetailView):
    model = Pasien
    template_name = 'masterdata/pasien-detail-view.html'
    context_object_name = 'pasien'

@method_decorator(login_required, name='dispatch')
class PasienUpdateView(UpdateView):
    model = Pasien
    template_name = 'masterdata/pasien-update-view.html'
    form_class = PasienForm
    success_url = reverse_lazy('masterdata:pasien-list')

@method_decorator(login_required, name='dispatch')
class PasienCreateView(CreateView):
    model = Pasien
    template_name = 'masterdata/pasien-create-view.html'
    form_class = PasienForm
    success_url = reverse_lazy('masterdata:pasien-list')


@login_required
def pasienCreateJsView(request):
    data = dict()
    form = PasienForm(request.POST or None)
    if request.method == 'POST':
        data['form_is_valid'] = False
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            # return redirect('masterdata:pasien-list')

    content = {
        'form': form,
    }
    data['html'] = render_to_string(
        'masterdata/includes/partial-pasien-create-view.html',
        content,
        request=request
    )
    return JsonResponse(data)

@method_decorator(login_required, name='dispatch')
class ObatListView(ListView):
    model = Obat
    template_name = 'masterdata/obat-list-view.html'
    context_object_name = 'obat_list'
    paginate_by = 10

    def get_context_data(self):
        context = super().get_context_data()
        context['url_create'] = reverse_lazy('masterdata:obat-create')
        return context

@method_decorator(login_required, name='dispatch')
class ObatDetailView(DetailView):
    model = Obat
    template_name = 'masterdata/obat-detail-view.html'
    context_object_name = 'obat'

@method_decorator(login_required, name='dispatch')
class ObatCreateView(CreateView):
    model = Obat
    template_name = 'masterdata/obat-create-view.html'
    form_class = ObatForm
    success_url = reverse_lazy('masterdata:obat-list')

@method_decorator(login_required, name='dispatch')
class ObatUpdateView(UpdateView):
    model = Obat
    template_name = 'masterdata/obat-update-view.html'
    form_class = ObatForm
    success_url = reverse_lazy('masterdata:obat-list')

@method_decorator(login_required, name='dispatch')
class TarifListView(ListView):
    model = Tarif
    template_name = 'masterdata/tarif-list-view.html'
    context_object_name = 'tarif_list'
    paginate_by = 10

    def get_queryset(self):
        query = super().get_queryset()
        q = self.request.GET.get('q', None)
        if q:
            query = query.annotate(
                search=SearchVector('nm_tarif', 'deskripsi')
            ).filter(search=q)

        return query

    def get_context_data(self):
        context = super().get_context_data()
        context['url'] = reverse_lazy('masterdata:tarif-create')
        return context

@method_decorator(login_required, name='dispatch')
class TarifDetailView(DetailView):
    model = Tarif
    template_name = 'masterdata/tarif-detail-view.html'
    context_object_name = 'tarif'

@method_decorator(login_required, name='dispatch')
class TarifCreateView(CreateView):
    model = Tarif   
    template_name = 'masterdata/tarif-create-view.html'
    form_class = TarifFrom
    success_url = reverse_lazy('masterdata:tarif-list')
    
@method_decorator(login_required, name='dispatch')
class TarifUpdateView(UpdateView):
    model = Tarif
    template_name = 'masterdata/tarif-update-view.html'
    form_class = TarifFrom
    success_url = reverse_lazy('masterdata:tarif-list')


def addStokObat(request, id):
    obat = get_object_or_404(Obat, pk=id)
    data = dict()
    form = StockObatForm(request.POST or None)
    if request.method == 'POST':
        data['form_is_valid'] = False
        if form.is_valid():
            obat.stok += form.cleaned_data['stok']
            obat.save()
            data['form_is_valid'] = True
            data['obj_id'] = obat.id
            data['data_html'] = render_to_string(
                'masterdata/includes/partial-obat-row.html',
                {'obat': obat}, request=request
            )

    content = {
        'form': form,
        'obat': obat,
    }
    data['html'] = render_to_string(
        'masterdata/includes/partial-add-stok-obat-view.html',
        content, request=request
    )
    return JsonResponse(data)