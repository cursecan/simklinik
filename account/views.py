from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.postgres.search import SearchVector
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import (
    CreateView, ListView, UpdateView,
)

from .models import User
from .forms import (
    UserCreateForm, UserSimpleForm
)


@method_decorator(login_required, name='dispatch')
class UserCreateView(CreateView):
    model = User
    form_class = UserCreateForm
    template_name = 'account/user-create-view.html'
    success_url = reverse_lazy('account:user-list')

@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    model = User
    form_class = UserSimpleForm
    template_name = 'account/user-update-view.html'
    success_url = reverse_lazy('account:user-list')

@method_decorator(login_required, name='dispatch')
class UserListView(ListView):
    template_name = 'account/user-list-view.html'
    context_object_name = 'user_list'
    paginate_by = 10

    def get_queryset(self):
        query = User.objects.exclude(id=1)
        q = self.request.GET.get('q', None)
        if q:
            query = query.annotate(
                search=SearchVector('username', 'first_name', 'email')
            ).filter(search=q)

        return query

    def get_context_data(self):
        context = super().get_context_data()
        context['url'] = reverse_lazy('account:user-create')
        return context


@login_required
def userResetPasswordView(request):
    form = PasswordChangeForm(request.user, data=request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('account:user-list')

    content = {
        'form': form
    }
    return render(request, 'account/password-reset-view.html', content)