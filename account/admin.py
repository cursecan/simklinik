from django.contrib import admin

from .models import User, UserLevel

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass

@admin.register(UserLevel)
class UserLevelAdmin(admin.ModelAdmin):
    pass