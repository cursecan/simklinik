from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from .models import User, Member

class UserSimpleForm(forms.ModelForm):
    tgl_lahir = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y'),
        input_formats=('%d/%m/%Y', )
    )
    
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'tgl_lahir', 'tmp_lahir',
            'jns_kelamin', 'telepon',
        ]

class UserCreateForm(forms.ModelForm):
    username = forms.SlugField(max_length=20)
    nama_lengkap = forms.CharField(max_length=50)

    class Meta:
        model = User
        fields = [
            'username', 'nama_lengkap', 'user_lvl',
        ]

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError('Username telah digunakan.')
        return username
        
    @transaction.atomic
    def save(self):
        user = User.objects.create_user(
            username = self.cleaned_data.get('username').lower(),
            first_name = self.cleaned_data.get('nama_lengkap'),
            password = '123123',
            user_lvl = self.cleaned_data.get('user_lvl')
        )
        Member.objects.create(user=user)

        return user