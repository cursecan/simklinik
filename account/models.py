from django.db import models
from django.contrib.auth.models import AbstractUser

import uuid

class User(AbstractUser):
    PRIA = 'P'
    WANITA = 'W'
    KELAMIN_LIST = (
        (PRIA, 'Pria'),
        (WANITA, 'Wanita')
    )
    guid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    user_lvl = models.ForeignKey('UserLevel', on_delete=models.CASCADE, related_name='users', blank=True, null=True)
    tgl_lahir = models.DateField(blank=True, null=True)
    tmp_lahir = models.CharField(max_length=100, blank=True)
    jns_kelamin = models.CharField(max_length=1, choices=KELAMIN_LIST, default=PRIA)
    telepon = models.CharField(max_length=16, blank=True)


class UserLevel(models.Model):
    code = models.CharField(max_length=3, unique=True)
    title = models.CharField(max_length=20)

    class Meta:
        ordering = [
            'code'
        ]

    def __str__(self):
        return '{} - {}'.format(self.code, self.title)


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        ordering = [
            'user__first_name', 'id'
        ]

    def __str__(self):
        return self.user.first_name