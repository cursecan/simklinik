from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

app_name = 'account'

urlpatterns = [
    path('user-create/', login_required(views.UserCreateView.as_view()), name='user-create'),
    path('user-reset-password/', login_required(views.userResetPasswordView), name='reset-password'),
    path('user/', login_required(views.UserListView.as_view()), name='user-list'),
    path('user/<int:pk>/', login_required(views.UserUpdateView.as_view()), name='user-update'),
]