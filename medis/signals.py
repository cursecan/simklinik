from django.db.models.signals import post_save, pre_delete
from django.db.models import F
from django.dispatch import receiver

from .models import ResepObat

from masterdata.models import Obat

@receiver(post_save, sender=ResepObat)
def triggering_used_obat(sender, instance, created, **kwargs):
    if created:
        Obat.objects.filter(id=instance.obat_id).update(
            stok = F('stok') - instance.jml_obat
        )

@receiver(pre_delete, sender=ResepObat)
def trigering_pre_delete_obat(sender, instance, **kwargs):
    Obat.objects.filter(id=instance.obat_id).update(
        stok = F('stok') + instance.jml_obat
    )
