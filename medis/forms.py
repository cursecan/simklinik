from django import forms

from .models import (
    RekamMedis, TindakanMedis, ResepObat,
)

from account.models import (
    Member,
)


class CreateRekamMedisForm(forms.ModelForm):
    tgl_rekam = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y'),
        input_formats=('%d/%m/%Y', )
    )
    dokter = forms.ModelChoiceField(
        queryset = Member.objects.filter(user__user_lvl__code='100') 
    )
    
    class Meta:
        model = RekamMedis
        fields = [
            'pasien', 'keluhan', 'dokter',
            'tgl_rekam',
        ]

    

class RekamMedisSimpleForm(forms.ModelForm):
    class Meta:
        model = RekamMedis
        fields = [
            'keluhan', 'dokter',
        ]

class RekamMedisForm(forms.ModelForm):
    class Meta:
        model = RekamMedis
        fields = [
            'keluhan',
            'anamnesis',
            'pemeriksaan_fsk',
            'diagnosis',
            'tensi_darah',
            'suhu_badan',
            'berat_badan',
            'bmi',
        ]


class TindakanMedisForm(forms.ModelForm):
    class Meta:
        model = TindakanMedis
        fields = [
            'tindakan', 'keterangan',
        ]

class ResepObatForm(forms.ModelForm):
    class Meta:
        model = ResepObat
        fields = [
            'obat', 'jml_obat'
        ]