from django.urls import path

from . import views

app_name = 'medis'

urlpatterns = [
    path('peserta/', views.PesertaListView.as_view(), name='peserta-list'),
    path('create-peserta/', views.PesertaCreateView.as_view(), name='peserta-create'),
    path('peserta/<int:pk>/', views.pesertaDetailView, name='peserta-detail'),
    path('peserta/<int:pk>/update/', views.PesertaUpdateView.as_view(), name='peserta-update'),

    path('rekam/<int:id>/tindakan-create-js/', views.tindakanMedisJsCreateView, name='tindakan-create-js'),
    path('tindakan-js/<int:id>/delete/', views.tindakanMedisDeleteJsView, name='tindakan-delete-js'),
    
    path('rekam/<int:id>/resep-create-js/', views.resepObatCreatejsView, name='resepobat-create-js'),
    path('resep-obat/<int:id>/delete/', views.resepObatDeletejsView, name='resepobat-delete-js'),
]