from django.apps import AppConfig


class MedisConfig(AppConfig):
    name = 'medis'

    def ready(self):
        from . import signals
