from django.db import models

from core.models import ComondBase

from masterdata.models import (
    Pasien, Tarif, Obat,
)
from account.models import (
    Member,
)

import uuid
from datetime import date

class RekamMedis(ComondBase):
    PROSES_ANTRI = 1
    PEMERIKSAAN = 2
    PROSES_BAYAR = 3
    SELESAI = 4
    BATAL = 9
    STATUSLIST = (
        (PROSES_ANTRI, 'Proses Antrian'),
        (PEMERIKSAAN, 'Periksa Dokter'),
        (PROSES_BAYAR, 'Proses Bayar'),
        (SELESAI, 'Selesai'),
        (BATAL, 'Batal')
    )
    guid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    tgl_rekam = models.DateField(default=date.today)
    pasien = models.ForeignKey(Pasien, on_delete=models.CASCADE, related_name='rekam_medis') 
    keluhan = models.CharField(max_length=200)
    anamnesis = models.TextField(max_length=2000, blank=True)
    pemeriksaan_fsk = models.TextField(max_length=2000, blank=True)
    diagnosis = models.CharField(max_length=200, blank=True)
    tensi_darah = models.CharField(max_length=10, blank=True)
    suhu_badan = models.CharField(max_length=10, blank=True)
    berat_badan = models.CharField(max_length=10, blank=True)
    bmi = models.CharField(max_length=10, blank=True)
    dokter = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='rm_dokter')
    state = models.PositiveSmallIntegerField(choices=STATUSLIST, default=PROSES_ANTRI, editable=False)
    
    class Meta:
        ordering = [
            'tgl_rekam', 'id'
        ]

    def __str__(self):
        return str(self.guid)


class TindakanMedis(ComondBase):
    rk_medis = models.ForeignKey(RekamMedis, on_delete=models.CASCADE, related_name='tindakan_medis')
    tindakan = models.ForeignKey(Tarif, on_delete=models.CASCADE, related_name='tindakan_medis')
    keterangan = models.CharField(max_length=100, blank=True)
    subtotal = models.PositiveIntegerField(default=0, editable=False)

    class Meta:
        ordering = [
            'tindakan__nm_tarif', 'id'
        ]

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        self.subtotal = self.tindakan.get_subtotal()
        super().save(*args, **kwargs)


class ResepObat(ComondBase):
    rk_medis = models.ForeignKey(RekamMedis, on_delete=models.CASCADE, related_name='resep_obat')
    obat = models.ForeignKey(Obat, on_delete=models.CASCADE, related_name='resep_obat')
    jml_obat = models.PositiveSmallIntegerField(default=1)
    subtotal = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = [
            'id',
        ]

    def __str__(self):
        return str(self.obat)


    def save(self, *args, **kwargs):
        self.subtotal = self.jml_obat * self.obat.harga_jual
        super().save(*args, **kwargs)

    class Meta:
        ordering = [
            'id',
        ]

    def __str__(self):
        return str(self.obat)