from django.core.exceptions import PermissionDenied
from .models import RekamMedis

def dokter_entry(function):
    def wrap(request, *args, **kwargs):
        entry = RekamMedis.objects.get(pk=kwargs['pk'])
        if entry.dokter == request.user.dockter:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap