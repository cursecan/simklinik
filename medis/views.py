from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView, CreateView, UpdateView, DetailView,
)
from django.views.generic.edit import ModelFormMixin

from .models import (
    RekamMedis, TindakanMedis, ResepObat
)
from .forms import (
    CreateRekamMedisForm, RekamMedisForm,
    TindakanMedisForm, ResepObatForm, RekamMedisSimpleForm
)
from core.decorators import isDokter

from payment.models import Invois


@method_decorator(login_required, name='dispatch')
class PesertaListView(ListView):
    context_object_name = 'list_peserta'
    template_name = 'medis/peserta-list-view.html'
    paginate_by = 10

    def get_queryset(self):
        queryset = RekamMedis.objects.filter(
            state__in=[RekamMedis.PROSES_ANTRI, RekamMedis.PEMERIKSAAN],
        )
        if self.request.user.user_lvl == '100':
            queryset = queryset.filter(dokter=self.request.user.member)
        return queryset

    def get_context_data(self):
        context = super().get_context_data()
        context['current_list'] = RekamMedis.objects.filter(
            tgl_rekam = timezone.now().date()
        )
        context['url'] = reverse_lazy('medis:peserta-create')
        return context

@method_decorator(login_required, name='dispatch')
class PesertaCreateView(CreateView):
    model = RekamMedis
    form_class = CreateRekamMedisForm
    template_name = 'medis/peserta-create-view.html'
    success_url = reverse_lazy('medis:peserta-list')

@method_decorator(login_required, name='dispatch')
class PesertaUpdateView(UpdateView):
    queryset = RekamMedis.objects.filter(state=RekamMedis.PROSES_ANTRI)
    form_class = RekamMedisSimpleForm
    template_name = 'medis/peserta-update-view.html'
    success_url = reverse_lazy('medis:peserta-list')

@login_required
@isDokter
def pesertaDetailView(request, pk):
    rekam_medis_obj = get_object_or_404(
        RekamMedis, pk=pk, 
        state__in=[RekamMedis.PROSES_ANTRI, RekamMedis.PEMERIKSAAN]
    )
    form = RekamMedisForm(request.POST or None, instance=rekam_medis_obj)
    if request.method == 'POST':
        if rekam_medis_obj.resep_obat.exists() and rekam_medis_obj.tindakan_medis.exists():
            if form.is_valid():
                instance = form.save(commit=False)
                instance.state=RekamMedis.PROSES_BAYAR
                instance.save()
                Invois.objects.get_or_create(rk_medis=instance)
                return redirect('medis:peserta-list')
        else:
            pass

    content = {
        'peserta': rekam_medis_obj,
        'form': form,
        'tindakan_list': rekam_medis_obj.tindakan_medis.all(),
        'resep_list': rekam_medis_obj.resep_obat.all(),
    }
    return render(request, 'medis/peserta-detail-view.html', content)

@login_required
def tindakanMedisJsCreateView(request, id):
    rk_medis_obj = get_object_or_404(RekamMedis, pk=id)
    data = dict()
    form = TindakanMedisForm(request.POST or None)
    if request.method == 'POST':
        data['form_is_valid'] = False
        if form.is_valid():
            instance = form.save(commit=False)
            instance.rk_medis = rk_medis_obj
            instance.save()
            data['form_is_valid'] = True

            tindakan_objs = rk_medis_obj.tindakan_medis.all()
            data['data_html'] = render_to_string(
                'medis/includes/partial-tindakan-medis-list.html',
                {'tindakan_list': tindakan_objs}, request=request
            )

    content = {
        'form': form,
        'peserta': rk_medis_obj,
    }
    data['html'] = render_to_string(
        'medis/includes/partial-tindakan-medis-create-view.html',
        content, request=request
    )    
    return JsonResponse(data)

@login_required
def tindakanMedisDeleteJsView(request, id):
    tindakan_obj = get_object_or_404(TindakanMedis, pk=id)
    rk_medis = tindakan_obj.rk_medis
    data = dict()
    content = {
        'tindakan': tindakan_obj
    }
    if request.method == 'POST':
        tindakan_obj.delete()
        data['data_html'] = render_to_string(
            'medis/includes/partial-tindakan-medis-list.html',
            {'tindakan_list': rk_medis.tindakan_medis.all()}, request=request
        )
        data['form_is_valid'] = True
    else:
        data['html'] = render_to_string(
            'medis/includes/partial-tindakan-medis-delete-view.html',
            content, request=request
        )
    return JsonResponse(data)

@login_required
def resepObatCreatejsView(request, id):
    rk_medis = get_object_or_404(RekamMedis, pk=id)
    data = dict()
    form = ResepObatForm(request.POST or None)
    if request.method == 'POST':
        data['form_is_valid'] = False
        if form.is_valid():
            instance = form.save(commit=False)
            instance.rk_medis = rk_medis
            instance.save()

            data['form_is_valid'] = True
            data['data_html'] = render_to_string(
                'medis/includes/partial-resep-obat-list.html',
                {'resep_list': rk_medis.resep_obat.all()}, request=request
            )

    content = {
        'form': form,
        'peserta': rk_medis
    }
    data['html'] = render_to_string(
        'medis/includes/partial-resep-obat-create-view.html',
        content, request=request
    )
    return JsonResponse(data)

@login_required
def resepObatDeletejsView(request, id):
    resep_obj = get_object_or_404(ResepObat, pk=id)
    data = dict()
    rk_medis = resep_obj.rk_medis
    content = {
        'resep': resep_obj,
    }
    if request.method == 'POST':
        resep_obj.delete()
        data['data_html'] = render_to_string(
            'medis/includes/partial-resep-obat-list.html',
            {'resep_list': rk_medis.resep_obat.all()}, request=request
        )
        data['form_is_valid'] = True
    else:
        data['html'] = render_to_string(
            'medis/includes/partial-resep-obat-delete-view.html',
            content, request=request
        )
    return JsonResponse(data)