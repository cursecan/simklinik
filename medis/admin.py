from django.contrib import admin

from .models import (
    RekamMedis, TindakanMedis, ResepObat,
)


@admin.register(RekamMedis)
class RekamMedisAdmin(admin.ModelAdmin):
    pass


@admin.register(TindakanMedis)
class TindakanMedisAdmin(admin.ModelAdmin):
    pass



@admin.register(ResepObat)
class ResepObatAdmin(admin.ModelAdmin):
    pass