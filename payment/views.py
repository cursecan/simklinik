from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from django.db.models import Sum, F, ExpressionWrapper, IntegerField
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from medis.models import RekamMedis

from .models import (
    Invois, Pay
)

from .forms import (
    PayForm,
)

@method_decorator(login_required, name='dispatch')
class InvoisListView(ListView):
    model = Invois
    template_name = 'payment/invois-list-view.html'
    paginate_by = 10
    context_object_name = 'invois_list'

@login_required
def invoisDetailView(request, id):
    invois = get_object_or_404(Invois, pk=id)
    form = PayForm(request.POST or None)
    content = {
        'invois': invois,
        'form': form,
    }
    return render(request, 'payment/invois-detail-view.html', content)

@login_required
def paymentView(request, inv_id):
    invois = get_object_or_404(Invois, pk=inv_id, state=Invois.BLMLUNAS)
    form = PayForm(request.POST)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.invois = invois
        instance.save()
        
    return redirect('payment:invois-detail', inv_id)

