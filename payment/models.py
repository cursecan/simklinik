from django.db import models
from django.utils import timezone

from core.models import ComondBase
from medis.models import RekamMedis

class Invois(ComondBase):
    BLMLUNAS = 1
    LUNAS = 2
    STATECHOICE = (
        (BLMLUNAS, 'Belum Lunas'),
        (LUNAS, 'Lunas'),
    )
    inv_num = models.CharField(max_length=20, unique=True, editable=False)
    rk_medis = models.ForeignKey(RekamMedis, on_delete=models.CASCADE, related_name='payment')
    amount = models.PositiveIntegerField(default=0)
    state = models.PositiveSmallIntegerField(choices=STATECHOICE, default=BLMLUNAS, editable=False)

    class Meta:
        ordering = [
            'id'
        ]
    
    def save(self, *args, **kwargs):
        if self.inv_num is None or self.inv_num == '':
            self.inv_num = timezone.now().strftime('%y%m%d%H%M%S')
        super().save(*args, **kwargs)

    def __str__(self):
        return '<Invois %r>' %self.id


class Pay(ComondBase):
    invois = models.ForeignKey(Invois, on_delete=models.CASCADE, related_name='paid')
    cash = models.IntegerField()
    balance = models.IntegerField(default=0, editable=False)

    class Meta:
        ordering = [
            'id'
        ]

    def save(self, *args, **kwargs):
        self.balance = self.invois.amount - self.cash
        super().save(*args, **kwargs)

    def __str__(self):
        return '<Pay %r>' %self.id