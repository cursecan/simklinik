from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Sum, F, IntegerField, ExpressionWrapper

from .models import (
    Invois, Pay
)

from medis.models import (
    RekamMedis, ResepObat, TindakanMedis
)


@receiver(post_save, sender=Invois)
def generate_cash_amount(sender, instance, created, **kwargs):
    if created:
        instance.amount = RekamMedis.objects.filter(id=instance.rk_medis_id).aggregate(
            t = ExpressionWrapper(
                Sum(F('resep_obat__subtotal') + F('tindakan_medis__subtotal')), output_field=IntegerField()
            )
        )['t']

        instance.save()

@receiver(post_save, sender=Pay)
def generate_state_payment(sender, instance, created, **kwargs):
    if created:
        Invois.objects.filter(id=instance.invois_id).update(
            state = Invois.LUNAS
        )
        RekamMedis.objects.filter(
            payment__id=instance.invois_id
        ).update(state=RekamMedis.SELESAI)