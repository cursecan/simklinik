from django import forms

from .models import (
    Pay,
)

class PayForm(forms.ModelForm):
    class Meta:
        model = Pay
        fields = [
            'cash',
        ]