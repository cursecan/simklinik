from django.urls import path

from . import views

app_name = 'payment'
urlpatterns = [
    path('invoices/', views.InvoisListView.as_view(), name='invois-list'),
    path('proses/<int:id>/', views.invoisDetailView, name='invois-detail'),
    path('pay/<int:inv_id>/', views.paymentView, name='paid'),
]