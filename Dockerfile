FROM python:3.6-alpine
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir /simklinik
RUN mkdir /simklinik/src
RUN mkdir /simklinik/env_root
RUN mkdir /simklinik/env_root/static_root

WORKDIR /simklinik/src
COPY . /simklinik/src/

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev \
    && rm -rf /var/cache/apk/*

RUN pip install -U pip && pip install -r requirements.txt